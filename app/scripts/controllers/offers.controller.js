(function(){

	'use strict';

	angular.module('AppFoodPoint.controllers')
		.controller('offersCtrl', offersCtrl);

	offersCtrl.$inject = ['requestOfertas', '$ionicSideMenuDelegate', '$ionicListDelegate'];

	function offersCtrl(requestOfertas, $ionicSideMenuDelegate, $ionicListDelegate) {

		var vm = this;

		vm.ofertas = requestOfertas.data;

		vm.favoritePlace = favoritePlace;


		///////////////////////

		$ionicSideMenuDelegate.canDragContent(false);


		// favorite place
		function favoritePlace(placeID) {
			console.log('favoritar');
			$ionicListDelegate.closeOptionButtons();
			// $http.post('xxxxx.bmc', {id: placeID}).then(doneFavorited, failCallbacks);
		}

	}

})();