(function() {

	'use strict';

	angular.module('AppFoodPoint.controllers')
		.controller('mapsCtrl', mapsCtrl);

	mapsCtrl.$inject = ['$scope', '$compile', '$http', '$ionicLoading', '$ionicSideMenuDelegate', '$ionicModal', '$ionicListDelegate'];

	function mapsCtrl($scope, $compile, $http, $ionicLoading, $ionicSideMenuDelegate, $ionicModal, $ionicListDelegate) {

		var vm = this,
			myloc, currentBound;

		vm.disableMap    = disableMap;
		vm.centerOnMe    = centerOnMe;
		vm.findPlace     = findPlace;
		vm.clearSearch   = clearSearch;
		vm.goToAddress   = goToAddress;
		vm.clearHistory  = clearHistory;
		vm.favoritePlace = favoritePlace;
		vm.setDirections = setDirections;
		vm.teste = teste;

		// global variable
		$scope.historySearch = [];

		// global function
		$scope.showInfoPlace = showInfoPlace;
		$scope.closeFinder   = closeFinder;

		$ionicSideMenuDelegate.canDragContent(true);

		function teste() {

			console.log('teste');

		}

		////////////////////////////


		// Create the search address modal
		$ionicModal.fromTemplateUrl('template/searchModal.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		// When map is initialize
		$scope.$on('mapInitialized', function(event, map) {
			// maps is global
			$scope.map = map;

			vm.positions = [];

			// marker for current location
			// myloc = new google.maps.Marker({
			// 	clickable: false,
			// 	icon: 'images/icon_currentlocation.png',
			// 	zIndex: 999,
			// 	map: $scope.map
			// });

			// centraliza o mapa
			if (currentBound) {
				$scope.map.setCenter(currentBound.getCenter());
			} else {
				centerOnMe();
			}

			google.maps.event.addListener($scope.map, "idle", getDisplayBound);
		});

		function getDisplayBound() {
			currentBound = $scope.map.getBounds();

			if (!currentBound) return;

			var config = {
				params: {
					k1: currentBound.Ea.k,
					j1: currentBound.Ea.j,
					k2: currentBound.wa.k,
					j2: currentBound.wa.j
				}
			};

			$http.get('http://www.bmchost.com.br/markers.php', config).then(setMarkersPosition, failCallbacks);

			vm.positions = [];

		}

		function setMarkersPosition(response) {
			vm.positions = response.data;
		}

		// disable maps when change tab to list view
		function disableMap() {
			$scope.map = null;
		}

		function centerOnMe() {

			if (!$scope.map) return;

			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-d"></i> \
								<p>Carregando</p> \
							</div>'
			});

			// get current position
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

				// myloc.setPosition(pos);

				$scope.map.setZoom(16);
				$scope.map.setCenter(pos);
				$ionicLoading.hide();
			}, function(error) {
				$ionicLoading.hide();
				navigator.notification.alert(
					'Não foi possível obter sua localização.', // message
					null, // callback
					'Atenção', // title
					'OK' // buttonName
				);
			});
		}

		// go to location in history list
		function goToAddress(place) {
			closeFinder();
			$scope.map.setCenter(place.geometry.location);
			$scope.map.setZoom(16);
		}

		// show search modal
		function findPlace() {
			if (!$scope.map) return;
			$scope.modal.show();
		}

		// Triggered in the search modal to close it
		function closeFinder() {
			$scope.modal.hide();
		}

		// clear input text of search place
		function clearSearch() {
			vm.locationAddress = '';
		}

		// clear history array
		function clearHistory() {
			$scope.historySearch = [];
		}

		// show info window of marker
		function showInfoPlace(evt, marker) {
			vm.marker = marker;
			$scope.map.scope.showInfoWindow.apply(this, [evt, 'marker-info']);
		}

		// favorite place
		function favoritePlace(placeID) {
			console.log('favoritar');
			$ionicListDelegate.closeOptionButtons();
			// $http.post('xxxxx.bmc', {id: placeID}).then(doneFavorited, failCallbacks);
		}

		// favorite local place
		function doneFavorited() {
			console.log('favoritado com sucesso!');
		}

		// trace driving directions to place selected
		function setDirections(place) {

			google.maps.event.clearListeners($scope.map, "idle");

			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-d"></i> \
								<p>Calculando rota</p> \
							</div>'
			});

			navigator.geolocation.getCurrentPosition(function(position) {

				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

				$scope.map.setCenter(pos);
				$ionicLoading.hide();

				var directionsDisplay = new google.maps.DirectionsRenderer(),
					directionsService = new google.maps.DirectionsService();

				directionsDisplay.setMap($scope.map);

				function calcRoute() {

					var start = pos.k + "," + pos.D,
						end = place.lat + "," + place.lng;

					var request = {
						origin: start,
						destination: end,
						optimizeWaypoints: true,
						travelMode: google.maps.TravelMode.DRIVING
					};

					directionsService.route(request, function(response, status) {
						if (status == google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(response);
							google.maps.event.addListener($scope.map, "idle", getDisplayBound);
						}
					});
				}

				calcRoute();
			});
		}

		// Error from http request
		function failCallbacks(error) {
			console.log('erro dos brabo!');
		}

	}

})();