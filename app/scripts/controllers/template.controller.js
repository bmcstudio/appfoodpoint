(function() {

	'use strict';

	angular.module('AppFoodPoint.controllers')
		.controller('templateCtrl', templateCtrl);


	templateCtrl.$inject = ['requestTemplate', 'requestCardapio', '$scope', '$ionicSideMenuDelegate', '$ionicModal'];

	function templateCtrl(requestTemplate, requestCardapio, $scope, $ionicSideMenuDelegate, $ionicModal) {

		var vm = this;

		vm.flagPedido = false;

		vm.template = requestTemplate.data;
		vm.cardapio = requestCardapio.data;



		vm.teste = teste;

		function teste() {
			vm.flagPedido = !vm.flagPedido;
		}



		///////////////////////

		$ionicSideMenuDelegate.canDragContent(false);

		// Create the cardapio modal
		$ionicModal.fromTemplateUrl('template/cardapioModal.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.$on('onRepeatEnd', function(scope, element, attrs) {

			var mySwiper = new Swiper('.swiper-container', {
				mode: 'horizontal',
				pagination: '.pagination',
				loop: false
			});
		});


		vm.getItemHeight = function(item, index) {
			// return (index % 2) === 0 ? 41 : 76;
			return (item.isDivider) ? 40 : 76;
		};


	}

})();