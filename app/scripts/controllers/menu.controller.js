(function(){

	'use strict';

	angular.module('AppFoodPoint.controllers')
		.controller('menuCtrl', menuCtrl);

	function menuCtrl() {

		var vm = this;

		vm.clearSearch = clearSearch;

		///////////////////////


		function clearSearch() {
			vm.search = '';
		}
	}

})();