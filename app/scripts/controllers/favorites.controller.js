(function() {

	'use strict';

	angular.module('AppFoodPoint.controllers')
		.controller('favoritesCtrl', favoritesCtrl);

	favoritesCtrl.$inject = ['requestFavorites', '$http', '$ionicSideMenuDelegate', '$ionicActionSheet'];

	function favoritesCtrl(requestFavorites, $http, $ionicSideMenuDelegate, $ionicActionSheet) {

		var vm = this;

		vm.favorites = requestFavorites.data;

		vm.removeItem  = removeItem;
		vm.limitText   = limitText;
		vm.clearSearch = clearSearch;

		vm.teste = teste;

		function teste () {
			console.log('teste');
		}

		///////////////////////

		$ionicSideMenuDelegate.canDragContent(false);
		// $http.get('scripts/favorites.json').then(setAllFavorites, failCallbacks);

		function setAllFavorites(response) {
			vm.favorites = response.data;
		}

		function removeItem(favorite) {

			$ionicActionSheet.show({
				destructiveText : 'Remover',
				titleText       : 'Deseja remover dos favoritos?',
				cancelText      : 'Cancelar',
				destructiveButtonClicked: function() {
					console.log('DESTRUCT');
					return true;
				}
			});
		}

		function limitText(currentText) {
			return currentText.trunc(25, true);
		}

		function clearSearch() {
			vm.search = '';
		}

		// Error from http request
		function failCallbacks(error) {
			console.log('erro dos brabo!');
		}

		String.prototype.trunc = function(n, useWordBoundary) {
			var toLong = this.length > n,
				s_ = toLong ? this.substr(0, n - 1) : this;
			s_ = useWordBoundary && toLong ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
			return toLong ? s_ + '...' : s_;
		};
	}

})();