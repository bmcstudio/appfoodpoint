(function() {

	'use strict';

	angular.module('AppFoodPoint.directives')
		.directive('ngRepeatEnd', ngRepeatEnd);

	ngRepeatEnd.$inject = ['$timeout'];

	function ngRepeatEnd($timeout) {

		var directive = {
			restrict: 'A',
			link: link
		};

		return directive;

		///////////////////////


		function link($scope, $element, $attrs, controller) {

			function init() {
				if ($scope.$last === true) {
	                $timeout(function () {
	                    $scope.$emit('onRepeatEnd');
	                });
	            }
			}

			// hack for wait DOM is ready
			$timeout(init, 0);
		}
	}

})();