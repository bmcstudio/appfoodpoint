(function() {

	'use strict';

	angular.module('AppFoodPoint.directives')
		.directive('googleAutocomplete', googlePlaces);

	googlePlaces.$inject = ['$timeout'];

	function googlePlaces($timeout) {

		var directive = {
			restrict: 'E',
			replace: true,
			require: 'ngModel',
			link: link,
			template: '<input type="text" class="input-block-level" ng-model="vm.locationAddress" placeholder="Digite um endereço..." />'
		};

		return directive;

		///////////////////////

		function link($scope, $element, $attrs, controller) {

			var init = function() {

				var input = $element[0],
					options = {
						country: 'br'
					},
					autocomplete = new google.maps.places.Autocomplete(input, options);

				autocomplete.bindTo('bounds', $scope.map);

				google.maps.event.addListener(autocomplete, 'place_changed', function() {

					var place = autocomplete.getPlace();

					if (!place.geometry) {
						return;
					}

					// If the place has a geometry, then present it on a map.
					if (place.geometry.viewport) {
						$scope.map.fitBounds(place.geometry.viewport);
					} else {
						$scope.map.setCenter(place.geometry.location);
						$scope.map.setZoom(16);  // Why 16? Because it looks good.
					}

					// put last address search in history array
					$scope.historySearch.push(place);

					// update ng-model
					$scope.$apply(function() {
						controller.$setViewValue($element.val());
					});

				});

				//this script force tap on item in google autocomplete list on mobile
				$timeout(function () {
					document.querySelector('div.pac-container').setAttribute('data-tap-disabled', 'true');

					var classname = document.getElementsByClassName("pac-container");

					for (var i = 0; i < classname.length; i++) {
						classname[i].addEventListener('click', function(event){
							// select item and close modal
							input.blur();
							$scope.closeFinder();
						}, false);
					}
				}, 500);
			};

			// hack for wait DOM is ready
			$timeout(init, 0);
		}
	}

})();