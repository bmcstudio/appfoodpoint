(function() {

    'use strict';

    angular.module('AppFoodPoint.services', []);
    angular.module('AppFoodPoint.directives', []);
    angular.module('AppFoodPoint.controllers', []);

    // Ionic Starter App, v0.9.20

    // angular.module is a global place for creating, registering and retrieving Angular modules
    // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
    // the 2nd parameter is an array of 'requires'
    // 'starter.services' is found in services.js
    // 'starter.controllers' is found in controllers.js
    angular.module('AppFoodPoint', ['ionic', 'config', 'ngMap', 'AppFoodPoint.httpInterceptor', 'AppFoodPoint.services', 'AppFoodPoint.directives', 'AppFoodPoint.controllers'])

    .run(function($ionicPlatform, $ionicTabsConfig) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });

        // Default to ios tab-bar style on android too
        $ionicTabsConfig.type = '';
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'menuCtrl as vm'
            })

            .state('app.tabs', {
                url: '/tabs',
                views:{
                    'menuContent':{
                        templateUrl: 'templates/tabs.html'
                    }
                }
            })

            .state('app.tabs.favorites', {
                url: '/favorites',
                views: {
                    'tab-favorites': {
                        templateUrl: 'templates/tab-favorites.html',
                        controller: 'favoritesCtrl as vm',
                        resolve: {
                            requestFavorites: function($http) {
                                return $http.get('scripts/favorites.json');
                            }
                        }
                    }
                }
            })

            .state('app.tabs.favorite', {
                url: '/favorite/:ID',
                views: {
                    'tab-favorites': {
                        templateUrl: 'templates/template.html',
                        controller: 'templateCtrl as vm',
                        resolve: {
                            requestTemplate: function($http, $stateParams) {
                                return $http.get('scripts/favorite.json', { params: { id: $stateParams.ID }});
                            },
                            requestCardapio: function($http, $stateParams) {
                                return $http.get('scripts/cardapio.json', { params: { id: $stateParams.ID }});
                            }
                        }
                    }
                }
            })

            .state('app.tabs.maps', {
                url: '/maps',
                views: {
                    'tab-maps': {
                        templateUrl: 'templates/tab-maps.html',
                        controller: 'mapsCtrl as vm'
                    }
                }
            })

            .state('app.tabs.mapfavorite', {
                url: '/mapfavorite/:ID',
                views: {
                    'tab-maps': {
                        templateUrl: 'templates/template.html',
                        controller: 'templateCtrl as vm',
                        resolve: {
                            requestTemplate: function($http, $stateParams) {
                                return $http.get('scripts/favorite.json', { params: { id: $stateParams.ID }});
                            },
                            requestCardapio: function($http, $stateParams) {
                                return $http.get('scripts/cardapio.json', { params: { id: $stateParams.ID }});
                            }
                        }
                    }
                }
            })

            .state('app.tabs.offers', {
                url: '/offers',
                views: {
                    'tab-offers': {
                        templateUrl: 'templates/tab-offers.html',
                        controller: 'offersCtrl as vm',
                        resolve: {
                            requestOfertas: function($http) {
                                return $http.get('scripts/ofertas.json');
                            }
                        }
                    }
                }
            })

            .state('app.tabs.offer', {
                url: '/offer/:ID',
                views: {
                    'tab-offers': {
                        templateUrl: 'templates/template.html',
                        controller: 'templateCtrl as vm',
                        resolve: {
                            requestTemplate: function($http, $stateParams) {
                                return $http.get('scripts/favorite.json', { params: { id: $stateParams.ID }});
                            },
                            requestCardapio: function($http, $stateParams) {
                                return $http.get('scripts/cardapio.json', { params: { id: $stateParams.ID }});
                            }
                        }
                    }
                }
            })

            .state('app.tabs.settings', {
                url: '/settings',
                views: {
                    'tab-settings': {
                        templateUrl: 'templates/tab-settings.html',
                        controller: 'settingsCtrl as vm'
                    }
                }
            });

            // .state('app.tabs.teste', {
            //     url: '/teste',
            //     views: {
            //         'tab-settings': {
            //             templateUrl: 'templates/teste.html'
            //         }
            //     }
            // });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/tabs/favorites');

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });

})();